<?php

namespace App\Service;

use App\Entity\Act;
use App\Entity\Catalog;
use App\Repository\ActRepository;
use App\Repository\CatalogRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\Callback;

class CatalogManager extends AbstractManager
{
    private $params;

    /** @var  CatalogRepository */
    protected $repository;

    /** @var  ActRepository */
    protected $actRepository;

    /** @var  ActManager */
    protected $actManager;

    public function __construct(
        EntityManagerInterface $em,
        CatalogRepository $repository,
        ParameterBagInterface $params
    ) {
        parent::__construct($em, $repository);
        $this->params = $params;
    }

    public function add($code, $name, $status, UploadedFile $file)
    {
        /** @var Catalog $catalog */
        $catalog = $this->repository->findOneBy(['code' => $code], ['added' => 'desc']);
        $fileName = tempnam($this->params->get('dir_files'), 'catalog');
        rename($file->getRealPath(), $fileName);

        if (is_null($catalog)) {
            $catalog = new Catalog($code, $name, $status, basename($fileName));
            $this->save($catalog);
        } else {
            $catalog->setName($name);
            $catalog->setStatus($status);
            $oldFile = $this->params->get('dir_files').DIRECTORY_SEPARATOR.$catalog->getFile();
            unlink($oldFile);
            $catalog->setFile(basename($fileName));
            $this->save($catalog);
        }

        return filesize($this->params->get('dir_files').DIRECTORY_SEPARATOR.$catalog->getFile());
    }

    public function getFile($code)
    {
        /** @var Catalog $catalog */
        $catalog = $this->repository->findOneBy(['code' => $code]);
        if (is_null($catalog)) {
            return false;
        }

        return $this->params->get('dir_files').DIRECTORY_SEPARATOR.$catalog->getFile();
    }

    public function getFileById($id)
    {
        /** @var Catalog $catalog */
        $catalog = $this->repository->find($id);

        return $this->params->get('dir_files').DIRECTORY_SEPARATOR.$catalog->getFile();
    }

    /**
     * @return string
     */
    public function getList()
    {
        /** @var Catalog $catalog */
        $catalogs = $this->repository->findAll();
        $str = '';
        foreach ($catalogs as $catalog) {
            $str .= sprintf(
                    '%s~%s~%s~%s',
                    $catalog->getCode(),
                    $catalog->getName(),
                    $catalog->getAdded()->format('Y-m-d H:i:s'),
                    $catalog->getStatus()
                ).'#';
        }

        return $str;
    }

}