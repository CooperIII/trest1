<?php

namespace App\Service;

use App\Entity\Act;
use App\Entity\Catalog;
use App\Repository\ActRepository;
use App\Repository\CatalogRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\DateTime;

class ActManager extends AbstractManager
{
    private $params;

    /** @var  ActRepository */
    protected $repository;

    public function __construct(EntityManagerInterface $em, ActRepository $repository, ParameterBagInterface $params)
    {
        parent::__construct($em, $repository);
        $this->params = $params;
    }

    public function add(array $lists, UploadedFile $file)
    {
        /** @var Act $act */
        $fileName = tempnam($this->params->get('dir_files'), 'act');
        rename($file->getRealPath(), $fileName);
        $act = new Act($lists, basename($fileName));
        $this->save($act);

        return file_exists($this->params->get('dir_files').DIRECTORY_SEPARATOR.$act->getFile());
    }

    public function getFile($id, $status)
    {
        /** @var Act $act */
        $act = $this->repository->find($id);
        if (is_null($act)) {
            return false;
        }

        return $this->params->get('dir_files').DIRECTORY_SEPARATOR.$act->getFile();
    }

    /**
     * @return string
     */
    public function getList()
    {
        $acts = $this->repository->findAll();
        $str = '';
        /** @var Act $act */
        foreach ($acts as $act) {
            $lists = [];
            $lists[0] = $act->getId();
            for ($i = 1; $i < 16; $i++) {
                $lists[$i] = $act->{'getList'.sprintf("%02d", $i)}();
            }
            $str .= vsprintf(
                    '%s~%s~%s~%s~%s~%s~%s~%s~%s~%s~%s~%s~%s~%s~%s~%s',
                    $lists
                ).'#';
        }

        return $str;
    }

    /**
     * @param integer $id
     */
    public function delete($id)
    {
        /** @var Act $act */
        $act = $this->repository->find($id);
        $file = $this->params->get('dir_files').DIRECTORY_SEPARATOR.$act->getFile();
        if (file_exists($file)) {
            unlink($file);
        }
        $this->remove($act);
        $this->flush();
    }
}