<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActRepository")
 * @ORM\Table(name="acts")
 */
class Act
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="list01", type="string", nullable=false)
     */
    protected $list01;

    /**
     * @ORM\Column(name="list02", type="string", nullable=false)
     */
    protected $list02;

    /**
     * @ORM\Column(name="list03", type="string", nullable=false)
     */
    protected $list03;

    /**
     * @ORM\Column(name="list04", type="string", nullable=false)
     */
    protected $list04;

    /**
     * @ORM\Column(name="list05", type="string", nullable=false)
     */
    protected $list05;

    /**
     * @ORM\Column(name="list06", type="string", nullable=false)
     */
    protected $list06;

    /**
     * @ORM\Column(name="list07", type="string", nullable=false)
     */
    protected $list07;

    /**
     * @ORM\Column(name="list08", type="string", nullable=false)
     */
    protected $list08;

    /**
     * @ORM\Column(name="list09", type="string", nullable=false)
     */
    protected $list09;

    /**
     * @ORM\Column(name="list10", type="string", nullable=false)
     */
    protected $list10;

    /**
     * @ORM\Column(name="list11", type="string", nullable=false)
     */
    protected $list11;

    /**
     * @ORM\Column(name="list12", type="string", nullable=false)
     */
    protected $list12;

    /**
     * @ORM\Column(name="list13", type="string", nullable=false)
     */
    protected $list13;

    /**
     * @ORM\Column(name="list14", type="string", nullable=false)
     */
    protected $list14;

    /**
     * @ORM\Column(name="list15", type="string", nullable=false)
     */
    protected $list15;

    /**
     * @ORM\Column(name="file", type="string", length=255, nullable=false)
     */
    protected $file;

    /**
     * Act constructor.
     *
     * @param array $lists
     * @param string $file
     */
    public function __construct($lists, $file)
    {
        $this->file = $file;

        foreach ($lists as $key => $list){
            $this->{'setList'.sprintf("%02d", $key)}($list);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return string
     */
    public function getList01()
    {
        return $this->list01;
    }

    /**
     * @param string $list01
     *
     * @return Act
     */
    public function setList01($list01)
    {
        $this->list01 = $list01;

        return $this;
    }

    /**
     * @return string
     */
    public function getList02()
    {
        return $this->list02;
    }

    /**
     * @param string $list02
     *
     * @return Act
     */
    public function setList02($list02)
    {
        $this->list02 = $list02;

        return $this;
    }

    /**
     * @return string
     */
    public function getList03()
    {
        return $this->list03;
    }

    /**
     * @param string $list03
     *
     * @return Act
     */
    public function setList03($list03)
    {
        $this->list03 = $list03;

        return $this;
    }

    /**
     * @return string
     */
    public function getList04()
    {
        return $this->list04;
    }

    /**
     * @param string $list04
     *
     * @return Act
     */
    public function setList04($list04)
    {
        $this->list04 = $list04;

        return $this;
    }

    /**
     * @return string
     */
    public function getList05()
    {
        return $this->list05;
    }

    /**
     * @param string $list05
     *
     * @return Act
     */
    public function setList05($list05)
    {
        $this->list05 = $list05;

        return $this;
    }

    /**
     * @return string
     */
    public function getList06()
    {
        return $this->list06;
    }

    /**
     * @param string $list06
     *
     * @return Act
     */
    public function setList06($list06)
    {
        $this->list06 = $list06;

        return $this;
    }

    /**
     * @return string
     */
    public function getList07()
    {
        return $this->list07;
    }

    /**
     * @param string $list07
     *
     * @return Act
     */
    public function setList07($list07)
    {
        $this->list07 = $list07;

        return $this;
    }

    /**
     * @return string
     */
    public function getList08()
    {
        return $this->list08;
    }

    /**
     * @param string $list08
     *
     * @return Act
     */
    public function setList08($list08)
    {
        $this->list08 = $list08;

        return $this;
    }

    /**
     * @return string
     */
    public function getList09()
    {
        return $this->list09;
    }

    /**
     * @param string $list09
     *
     * @return Act
     */
    public function setList09($list09)
    {
        $this->list09 = $list09;

        return $this;
    }

    /**
     * @return string
     */
    public function getList10()
    {
        return $this->list10;
    }

    /**
     * @param string $list10
     *
     * @return Act
     */
    public function setList10($list10)
    {
        $this->list10 = $list10;

        return $this;
    }

    /**
     * @return string
     */
    public function getList11()
    {
        return $this->list11;
    }

    /**
     * @param string $list11
     *
     * @return Act
     */
    public function setList11($list11)
    {
        $this->list11 = $list11;

        return $this;
    }

    /**
     * @return string
     */
    public function getList12()
    {
        return $this->list12;
    }

    /**
     * @param string $list12
     *
     * @return Act
     */
    public function setList12($list12)
    {
        $this->list12 = $list12;

        return $this;
    }

    /**
     * @return string
     */
    public function getList13()
    {
        return $this->list13;
    }

    /**
     * @param string $list13
     *
     * @return Act
     */
    public function setList13($list13)
    {
        $this->list13 = $list13;

        return $this;
    }

    /**
     * @return string
     */
    public function getList14()
    {
        return $this->list14;
    }

    /**
     * @param string $list14
     *
     * @return Act
     */
    public function setList14($list14)
    {
        $this->list14 = $list14;

        return $this;
    }

    /**
     * @return string
     */
    public function getList15()
    {
        return $this->list15;
    }

    /**
     * @param string $list15
     *
     * @return Act
     */
    public function setList15($list15)
    {
        $this->list15 = $list15;

        return $this;
    }


}
