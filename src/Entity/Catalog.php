<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CatalogRepository")
 * @ORM\Table(name="catalogs")
 */
class Catalog
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="text", nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(name="code", type="string", nullable=false)
     */
    protected $code;

    /**
     * @ORM\Column(name="added", type="datetime", nullable=true)
     */
    protected $added;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    protected $status; // 1 - on; 2 - off

    /**
     * @ORM\Column(name="file", type="string", length=255, nullable=false)
     */
    protected $file;


    public function __construct($code, $name, $status, $file)
    {
        $this->code = $code;
        $this->name= $name;
        $this->status = $status;
        $this->file = $file;

        $this->added = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getAdded(): ?\DateTimeInterface
    {
        return $this->added;
    }

    public function setAdded(?\DateTimeInterface $added): self
    {
        $this->added = $added;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

}
