<?php

namespace App\Controller;

use App\Service\ActManager;
use App\Service\CatalogManager;
use App\Service\UserManager;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FileController extends AbstractController
{
    private $catalogManager;

    private $userManager;

    private $actManager;

    public function __construct(CatalogManager $catalogManager, UserManager $userManager, ActManager $actManager)
    {
        $this->catalogManager = $catalogManager;
        $this->userManager = $userManager;
        $this->actManager = $actManager;
    }



    ////
    public function tester()
    {
        return new Response(
            '<html><body>
        <form action="/api/builds/1/upload" method="post"  enctype="multipart/form-data" >
          <div>
          <label>Загрузка КЦ на сервер</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="name" placeholder="name">
      <input type="text" name="short" placeholder="short">
      <input type="text" name="list" placeholder="list">
      <input type="text" name="status" placeholder="status">
   <input type="file" id="file" name="file">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>
        <form action="/api/builds/1/download" method="post">
          <div>
          <label>Загрузка КЦ с сервера</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="status" placeholder="status">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>

        <form action="/api/acts/1/101/upload" method="post"  enctype="multipart/form-data" >
          <div>
          <label>Загрузка С2Б на сервер</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="status" placeholder="status">
      <input type="text" name="month" placeholder="month">
   <input type="file" id="file" name="file">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>

        <form action="/api/acts/101/download" method="post">
          <div>
          <label>Загрузка С2Б с сервера</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="status" placeholder="status">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>


        <form action="/api/builds/list" method="post">
          <div>
          <label>Список КЦ</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="status" placeholder="status">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>
        
        <form action="/api/acts/1/list" method="post">
          <div>
          <label>Список С2Б</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="status" placeholder="status">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>
        </body></html>'
        );
    }




    public function catalogUpload($code, Request $request)
    {
        $info = print_r($request->request->all(), true);
        file_put_contents($this->getParameter('dir_files').DIRECTORY_SEPARATOR.'log.txt', $info);
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            //dump($request->files->get('file'));
            $size = $this->catalogManager->add(
                $code,
                $request->request->get('name'),
                $request->request->get('status'),
                $request->files->get('file')
            );

            return new Response($size, $size > 0 ? 200 : 400);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }

    public function catalogDownload($code, Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            $file = $this->catalogManager->getFile($code);

            if (empty($file) || file_exists($file) === false) {
                return new Response('file not found', 404);
            }

            return new BinaryFileResponse($file, 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }

    public function catalogList(Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }

        return new Response($this->catalogManager->getList(), 200);
    }


    public function actUpload(Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            $lists = [];
            for ($i=1; $i<16; $i++){
                $lists[$i] = $request->request->get('list'. sprintf("%02d", $i));
            }
           // dump($lists);
            $this->actManager->add($lists, $request->files->get('file'));

            return new Response('OK', 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }

    public function actDownload($id, Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            $file = $this->actManager->getFile($id, $request->request->get('status'));
            if (empty($file) || file_exists($file) === false) {
                return new Response('file not found', 404);
            }

            return new BinaryFileResponse($file, 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }

    public function actList(Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }

        return new Response($this->actManager->getList(), 200);
    }

    public function actDelete($id, Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            $this->actManager->delete($id);

            return new Response('OK', 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }

}